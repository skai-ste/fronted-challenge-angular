import {Component, ElementRef} from '@angular/core';
import {addClass, hasClass, removeClass} from "./utils/toggle-class/dom";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(private elementRef: ElementRef) {
    this.elementRef = elementRef;
  }
  onAccordionHeaderClick(event: Event) {
    const target: HTMLElement = event.currentTarget as HTMLElement;
    const nextSibling = target.nextElementSibling as HTMLElement;
    if(hasClass('hidden', nextSibling)) {
      removeClass('hidden', nextSibling)
      removeClass('closed', target);
    } else {
      addClass('closed', target);
      addClass('hidden', nextSibling)
    }
    return false;
  }
}
