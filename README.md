## Solution

I have fixed the issues mentioned in the description, but didn't have enough time to work on all accessibility problems.

I made the design responsive by using CSS grid. It currently switches at 600px. On smaller screens the layout matches the design and on wider screens the inputs are displayed side by side. 

## Further improvements

### Accessibility

These are some things that could still be improved, but due to time constraints I didn't manage to do:

* Make the accordion button work with keyboard.
* Currently the collapse icon is visible to screen reader, we should hide it. I was using `::after` pseudo element which makes it hard to do this as is.
* Not sure if using input placeholder is the best way to signify the type of the input. Maybe adding a label for each input would be better.
* If the form submission failed we should have error messages for each field that describes the problem e.g. “Email address is invalid”.

### Layout

Layout can still be improved (e.g. there is empty space at the end of the section), but it's a good starting point that is easy to change. Would be good to have some input from a designer on what is the best approach.

### Mistakes in the assignment

I have noticed few small things that could be improved in the assignment:
* The title of the repository has `froted` it should be `frontend`.
* In the provided design we have a field called "Phone CountryNumber". There should probably be two separate fields - "Phone Number" and "Country". I have made this change in my solution.
* Credit card section has "City" input, but that seems unusual - most of the time these forms don't ask you for your city.
